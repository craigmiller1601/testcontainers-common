package us.craigmiller160.testcontainers.common.core

import dasniko.testcontainers.keycloak.KeycloakContainer
import org.testcontainers.containers.MongoDBContainer
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.weaviate.WeaviateContainer

data class ContainerInitializationResult(
    val statuses: ContainerStatuses,
    val containers: Containers
)

data class ContainerStatuses(
    val postgres: ContainerStatus,
    val keycloak: ContainerStatus,
    val mongo: ContainerStatus,
    val weaviate: ContainerStatus
)

data class Containers(
    val postgres: PostgreSQLContainer<*>?,
    val keycloak: KeycloakContainer?,
    val mongo: MongoDBContainer?,
    val weaviate: WeaviateContainer?
)
