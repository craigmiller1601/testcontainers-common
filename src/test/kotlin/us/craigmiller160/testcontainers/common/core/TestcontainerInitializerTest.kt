package us.craigmiller160.testcontainers.common.core

import kotlin.test.assertEquals
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import us.craigmiller160.testcontainers.common.config.ContainerConfig
import us.craigmiller160.testcontainers.common.config.TestcontainersCommonConfig
import us.craigmiller160.testcontainers.common.core.TestcontainerConstants.WEAVIATE_API_KEY

class TestcontainerInitializerTest {
  companion object {
    private const val LOCAL_HOST = "localhost"
    private const val CI_HOST = "docker"
  }

  private var initResult: ContainerInitializationResult? = null

  @AfterEach
  fun stopContainers() {
    initResult?.containers?.keycloak?.stop()
    initResult?.containers?.mongo?.stop()
    initResult?.containers?.postgres?.stop()
    initResult?.containers?.weaviate?.stop()
    initResult = null
  }

  @Test
  fun `gets postgres schema name based on working directory`() {
    val schemaName = TestcontainerInitializer.getPostgresSchema()
    assertEquals("testcontainers_common", schemaName)
  }

  private fun validatePostgresProps() {
    assertThat(System.getProperty(TestcontainerConstants.POSTGRES_URL_PROP))
        .isEqualTo(initResult?.containers?.postgres?.jdbcUrl)
    assertThat(System.getProperty(TestcontainerConstants.POSTGRES_R2_URL_PROP))
        .isEqualTo(initResult?.containers?.postgres?.jdbcUrl?.replace(Regex("^jdbc"), "r2dbc"))
    assertThat(System.getProperty(TestcontainerConstants.POSTGRES_USER_PROP))
        .isEqualTo(initResult?.containers?.postgres?.username)
    assertThat(System.getProperty(TestcontainerConstants.POSTGRES_PASSWORD_PROP))
        .isEqualTo(initResult?.containers?.postgres?.password)
  }

  private fun validateKeycloakProps() {
    assertThat(System.getProperty(TestcontainerConstants.KEYCLOAK_URL_PROP))
        .isEqualTo(initResult?.containers?.keycloak?.authServerUrl?.replace(Regex("\\/$"), ""))
    assertThat(System.getProperty(TestcontainerConstants.POSTGRES_SCHEMA_PROP))
        .isEqualTo("testcontainers_common")
    assertThat(System.getProperty(TestcontainerConstants.KEYCLOAK_REALM_PROP))
        .isEqualTo(TestcontainerConstants.KEYCLOAK_REALM)
    assertThat(System.getProperty(TestcontainerConstants.KEYCLOAK_CLIENT_ID_PROP))
        .isEqualTo(TestcontainerConstants.KEYCLOAK_CLIENT_ID)
    assertThat(System.getProperty(TestcontainerConstants.KEYCLOAK_CLIENT_SECRET_PROP))
        .isEqualTo(TestcontainerConstants.KEYCLOAK_CLIENT_SECRET)
    assertThat(System.getProperty(TestcontainerConstants.KEYCLOAK_ADMIN_USER_PROP))
        .isEqualTo(TestcontainerConstants.KEYCLOAK_ADMIN_USER)
    assertThat(System.getProperty(TestcontainerConstants.KEYCLOAK_ADMIN_PASSWORD_PROP))
        .isEqualTo(TestcontainerConstants.KEYCLOAK_ADMIN_PASSWORD)
  }

  private fun validateMongoProps() {
    val url =
        if (isInCi()) {
          initResult?.containers?.mongo?.replicaSetUrl?.replace(LOCAL_HOST, CI_HOST)
        } else {
          initResult?.containers?.mongo?.replicaSetUrl
        }
    assertThat(System.getProperty(TestcontainerConstants.MONGO_URL_PROP)).isEqualTo(url)
  }

  private fun isInCi(): Boolean {
    return System.getenv("CI_PIPELINE_ID") != null
  }

  private fun validateWeaviateProps() {
    val host = if (isInCi()) CI_HOST else LOCAL_HOST
    val port = initResult?.containers?.weaviate?.getMappedPort(8080)!!
    assertThat(System.getProperty(TestcontainerConstants.WEAVIATE_HOST_PROP))
        .isEqualTo("$host:$port")
    assertThat(System.getProperty(TestcontainerConstants.WEAVIATE_SCHEME_PROP)).isEqualTo("http")
    assertThat(System.getProperty(TestcontainerConstants.WEAVIATE_API_KEY_PROP))
        .isEqualTo(WEAVIATE_API_KEY)
  }

  @Test
  fun `can initialize all containers`() {
    initResult =
        TestcontainerInitializer.initialize(
            TestcontainersCommonConfig(
                postgres = ContainerConfig(enable = true),
                keycloak = ContainerConfig(enable = true),
                mongo = ContainerConfig(enable = true),
                weaviate = ContainerConfig(enable = true)))
    assertThat(initResult?.statuses)
        .hasFieldOrPropertyWithValue("postgres", ContainerStatus.STARTED)
        .hasFieldOrPropertyWithValue("keycloak", ContainerStatus.STARTED)
        .hasFieldOrPropertyWithValue("mongo", ContainerStatus.STARTED)
        .hasFieldOrPropertyWithValue("weaviate", ContainerStatus.STARTED)

    validatePostgresProps()
    validateKeycloakProps()
    validateMongoProps()
    validateWeaviateProps()
  }

  @Test
  fun `can initialize postgres only`() {
    initResult =
        TestcontainerInitializer.initialize(
            TestcontainersCommonConfig(
                postgres = ContainerConfig(enable = true),
                keycloak = ContainerConfig(enable = false),
                mongo = ContainerConfig(enable = false),
                weaviate = ContainerConfig(enable = false)))
    assertThat(initResult?.statuses)
        .hasFieldOrPropertyWithValue("postgres", ContainerStatus.STARTED)
        .hasFieldOrPropertyWithValue("keycloak", ContainerStatus.DISABLED)
        .hasFieldOrPropertyWithValue("mongo", ContainerStatus.DISABLED)
        .hasFieldOrPropertyWithValue("weaviate", ContainerStatus.DISABLED)

    validatePostgresProps()
  }

  @Test
  fun `can initialize weaviate only`() {
    initResult =
        TestcontainerInitializer.initialize(
            TestcontainersCommonConfig(
                postgres = ContainerConfig(enable = false),
                keycloak = ContainerConfig(enable = false),
                mongo = ContainerConfig(enable = false),
                weaviate = ContainerConfig(enable = true)))
    assertThat(initResult?.statuses)
        .hasFieldOrPropertyWithValue("postgres", ContainerStatus.DISABLED)
        .hasFieldOrPropertyWithValue("keycloak", ContainerStatus.DISABLED)
        .hasFieldOrPropertyWithValue("mongo", ContainerStatus.DISABLED)
        .hasFieldOrPropertyWithValue("weaviate", ContainerStatus.STARTED)

    validateWeaviateProps()
  }

  @Test
  fun `can initialize keycloak only`() {
    initResult =
        TestcontainerInitializer.initialize(
            TestcontainersCommonConfig(
                postgres = ContainerConfig(enable = false),
                keycloak = ContainerConfig(enable = true),
                mongo = ContainerConfig(enable = false),
                weaviate = ContainerConfig(enable = false)))
    assertThat(initResult?.statuses)
        .hasFieldOrPropertyWithValue("postgres", ContainerStatus.DISABLED)
        .hasFieldOrPropertyWithValue("keycloak", ContainerStatus.STARTED)
        .hasFieldOrPropertyWithValue("mongo", ContainerStatus.DISABLED)
        .hasFieldOrPropertyWithValue("weaviate", ContainerStatus.DISABLED)

    validateKeycloakProps()
  }

  @Test
  fun `can initialize mongo only`() {
    initResult =
        TestcontainerInitializer.initialize(
            TestcontainersCommonConfig(
                postgres = ContainerConfig(enable = false),
                keycloak = ContainerConfig(enable = false),
                mongo = ContainerConfig(enable = true),
                weaviate = ContainerConfig(enable = false)))
    assertThat(initResult?.statuses)
        .hasFieldOrPropertyWithValue("postgres", ContainerStatus.DISABLED)
        .hasFieldOrPropertyWithValue("keycloak", ContainerStatus.DISABLED)
        .hasFieldOrPropertyWithValue("mongo", ContainerStatus.STARTED)
        .hasFieldOrPropertyWithValue("weaviate", ContainerStatus.DISABLED)

    validateMongoProps()
  }

  @Test
  fun `can initialize nothing`() {
    initResult =
        TestcontainerInitializer.initialize(
            TestcontainersCommonConfig(
                postgres = ContainerConfig(enable = false),
                keycloak = ContainerConfig(enable = false),
                mongo = ContainerConfig(enable = false),
                weaviate = ContainerConfig(enable = false)))
    assertThat(initResult?.statuses)
        .hasFieldOrPropertyWithValue("postgres", ContainerStatus.DISABLED)
        .hasFieldOrPropertyWithValue("keycloak", ContainerStatus.DISABLED)
        .hasFieldOrPropertyWithValue("mongo", ContainerStatus.DISABLED)
        .hasFieldOrPropertyWithValue("weaviate", ContainerStatus.DISABLED)
  }
}
