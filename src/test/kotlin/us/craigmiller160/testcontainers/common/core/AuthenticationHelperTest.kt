package us.craigmiller160.testcontainers.common.core

import java.util.UUID
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.keycloak.admin.client.KeycloakBuilder
import org.keycloak.representations.idm.RoleRepresentation
import us.craigmiller160.testcontainers.common.config.ContainerConfig
import us.craigmiller160.testcontainers.common.config.TestcontainersCommonConfig

class AuthenticationHelperTest {
  companion object {
    private lateinit var initResult: ContainerInitializationResult

    @BeforeAll
    @JvmStatic
    fun setup() {
      initResult =
          TestcontainerInitializer.initialize(
              TestcontainersCommonConfig(
                  postgres = ContainerConfig(enable = false),
                  mongo = ContainerConfig(enable = false),
                  keycloak = ContainerConfig(enable = true),
                  weaviate = ContainerConfig(enable = false)))
    }
  }

  private val keycloak =
      KeycloakBuilder.builder()
          .serverUrl(System.getProperty(TestcontainerConstants.KEYCLOAK_URL_PROP))
          .realm(AuthenticationHelper.ADMIN_REALM)
          .username(TestcontainerConstants.KEYCLOAK_ADMIN_USER)
          .password(TestcontainerConstants.KEYCLOAK_ADMIN_PASSWORD)
          .clientId(AuthenticationHelper.ADMIN_CLIENT_ID)
          .grantType(AuthenticationHelper.GRANT_TYPE_VALUE)
          .build()

  @Test
  fun `gets the various container properties`() {
    val port =
        initResult.containers.keycloak?.httpPort
            ?: throw IllegalStateException("Can't get port from container")

    val host =
        if (System.getenv("CI_PIPELINE_ID") != null) {
          "docker"
        } else {
          "localhost"
        }

    val url = AuthenticationHelper.containerUrl
    assertEquals("http://${host}:$port", url)

    val adminUsername = AuthenticationHelper.containerAdminUsername
    assertEquals(TestcontainerConstants.KEYCLOAK_ADMIN_USER, adminUsername)

    val adminPassword = AuthenticationHelper.containerAdminPassword
    assertEquals(TestcontainerConstants.KEYCLOAK_ADMIN_PASSWORD, adminPassword)

    val realm = AuthenticationHelper.containerRealm
    assertEquals(TestcontainerConstants.KEYCLOAK_REALM, realm)

    val clientId = AuthenticationHelper.containerClientId
    assertEquals(TestcontainerConstants.KEYCLOAK_CLIENT_ID, clientId)

    val clientSecret = AuthenticationHelper.containerClientSecret
    assertEquals(TestcontainerConstants.KEYCLOAK_CLIENT_SECRET, clientSecret)
  }

  @Test
  fun `can create a user in keycloak and then login with that user`() {
    val helper = AuthenticationHelper()
    val userName = "MyUser@gmail.com"
    val testUser = helper.createUser(userName)
    assertThat(testUser)
        .hasFieldOrPropertyWithValue("roles", listOf(AuthenticationHelper.ACCESS_ROLE))
    assertThat(testUser.userName).matches("^\\S+_$userName")

    val realm = keycloak.realm(TestcontainerConstants.KEYCLOAK_REALM)
    val users = realm.users().searchByUsername(testUser.userName, true)
    assertThat(users)
        .hasSize(1)
        .first()
        .hasFieldOrPropertyWithValue("username", testUser.userName.lowercase())

    val token = helper.login(testUser)
    assertNotNull(token)
  }

  @Test
  fun `can create a user in keycloak with additional roles`() {
    val helper = AuthenticationHelper()
    val userName = "MyUser@gmail.com"
    val roleName = "OtherRole_${UUID.randomUUID()}"

    val realm = keycloak.realm(TestcontainerConstants.KEYCLOAK_REALM)
    val kcClientId =
        realm.clients().findByClientId(TestcontainerConstants.KEYCLOAK_CLIENT_ID).first().id
    realm.clients().get(kcClientId).roles().create(RoleRepresentation().apply { name = roleName })

    val testUser = helper.createUser(userName, listOf(AuthenticationHelper.ACCESS_ROLE, roleName))
    assertThat(testUser)
        .hasFieldOrPropertyWithValue("roles", listOf(AuthenticationHelper.ACCESS_ROLE, roleName))
    assertThat(testUser.userName).matches("^\\S+_$userName")

    val roles =
        realm
            .users()
            .get(testUser.userId.toString())
            .roles()
            .clientLevel(kcClientId)
            .listAll()
            .map { it.name }
    assertThat(roles).contains(AuthenticationHelper.ACCESS_ROLE, roleName)
  }

  @Test
  fun `can create a new role in keycloak`() {
    val helper = AuthenticationHelper()
    val roleName = "MyRole_${UUID.randomUUID()}"
    helper.createRole(roleName)

    val realm = keycloak.realm(TestcontainerConstants.KEYCLOAK_REALM)
    val clientId =
        realm.clients().findByClientId(TestcontainerConstants.KEYCLOAK_CLIENT_ID).first().id

    assertDoesNotThrow { realm.clients().get(clientId).roles().get(roleName) }
  }

  @Test
  fun `will skip creating a new role in keycloak if the role already exists`() {
    val helper = AuthenticationHelper()
    val roleName = "MyRole_${UUID.randomUUID()}"

    val realm = keycloak.realm(TestcontainerConstants.KEYCLOAK_REALM)
    val clientId =
        realm.clients().findByClientId(TestcontainerConstants.KEYCLOAK_CLIENT_ID).first().id

    realm.clients().get(clientId).roles().create(RoleRepresentation().apply { name = roleName })

    helper.createRole(roleName)

    assertDoesNotThrow { realm.clients().get(clientId).roles().get(roleName) }
  }
}
